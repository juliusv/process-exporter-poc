# Docker Process Exporter Monitoring Demo

A quick proof of concept of Prometheus monitoring process-exporters in each Docker container.

## Endpoints:

- Grafana: port 3000
- Prometheus: port 9090

## Bringing everything up

    make run

Use `<Ctrl>+C` to stop everything.
